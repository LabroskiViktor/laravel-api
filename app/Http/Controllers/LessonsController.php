<?php

namespace App\Http\Controllers;

use App\Helpers\LessonTransformer;
use App\Http\Controllers\ApiController;
use App\Lesson;
use Illuminate\Http\Request;
use Validator;

class LessonsController extends ApiController
{
    /**
     * @var App\Helpers\LessonTransformer
     */
    protected $lessonTransformer;

    public function __construct(LessonTransformer $lessonTransformer)
    {
        $this->lessonTransformer = $lessonTransformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lessons = Lesson::all();

        return $this->respond([
            'data' => $this->lessonTransformer->transformCollection($lessons->toArray()),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title'     => 'required',
            'body'      => 'required',
            'some_bool' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return $this->respondValidationError($validator->errors());
        } else {
            $lesson = Lesson::create($request->all());
            return $this->respondSuccess('Lesson created');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $lesson = Lesson::find($id);

        if (!$lesson) {
            return $this->respondNotFound('Lesson does not exists');
        }

        return $this->respond([
            'data' => $this->lessonTransformer->transform($lesson),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $lesson = Lesson::find($id);
        if (!$lesson) {
            return $this->respondNotFound('Lesson does not exists');
        } else {
            $rules = [
                'title'     => 'required',
                'body'      => 'required',
                'some_bool' => 'required',
            ];

            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return $this->respondValidationError($validator->errors());
            } else {
                $lesson->update($request->all());
                return $this->respondSuccess('Lesson updated');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $lesson = Lesson::find($id);
        if (!$lesson) {
            return $this->respondNotFound('Lesson does not exists');
        } else {
            $lesson->delete();
            return $this->respondSuccess('Lesson deleted');
        }
    }
}
