<?php

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {
	/**
	 * Seed the application's database.
	 *
	 * @return void
	 */
	public function run() {
		DB::table('lessons')->truncate();
		$faker = Faker::create();
		foreach (range(1, 30) as $index) {
			DB::table('lessons')->insert([
				'title' => $faker->word(5),
				'body' => $faker->paragraph(4),
				'some_bool' => $faker->boolean(),
				'created_at' => $faker->dateTimeThisMonth()->format('Y-m-d H:i:s'),
				'updated_at' => $faker->dateTimeThisMonth()->format('Y-m-d H:i:s'),
			]);
		}
	}
}
